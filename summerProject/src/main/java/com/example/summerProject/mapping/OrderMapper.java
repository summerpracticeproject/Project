package com.example.summerProject.mapping;

import com.example.summerProject.model.Order;
import com.example.summerProject.model.OrderDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderDto toDto(Order order);

    Order backFromDto(OrderDto orderDto);
}