package com.example.summerProject.mapping;


import com.example.summerProject.model.Customer;
import com.example.summerProject.model.CustomerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {
    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(source = "customer.id", target = "id")
    @Mapping(source = "customer.name", target = "name")
    @Mapping(source = "customer.phone", target = "phone")
    CustomerDto toDto(Customer customer);

    @Mapping(source = "customerDto.id", target = "id")
    @Mapping(source = "customerDto.name", target = "name")
    @Mapping(source = "customerDto.phone", target = "phone")
    Customer backFromDto(CustomerDto customerDto);
}
