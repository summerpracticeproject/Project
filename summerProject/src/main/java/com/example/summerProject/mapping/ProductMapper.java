package com.example.summerProject.mapping;


import com.example.summerProject.model.Product;
import com.example.summerProject.model.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDto toDto(Product product);

    Product backFromDto(ProductDto productDto);
}
