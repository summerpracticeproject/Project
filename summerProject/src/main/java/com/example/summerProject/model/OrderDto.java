package com.example.summerProject.model;

import javax.persistence.*;

public class OrderDto {

    private Integer id;

    private Integer productID;

    private Integer customerID;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductID() {
        return this.productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public Integer getCustomerID () { return this.customerID; }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }
}
