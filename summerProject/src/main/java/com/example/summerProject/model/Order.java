package com.example.summerProject.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "_order")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Order {

    public Order(){}

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "orderIdSeq", sequenceName = "order_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orderIdSeq")
    private Integer id;

    @Column(name = "productid")
    private Integer productID;


    @Column(name = "customerid")
    private Integer customerID;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductID() {
        return this.productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public Integer getCustomerID () { return this.customerID; }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }
}
