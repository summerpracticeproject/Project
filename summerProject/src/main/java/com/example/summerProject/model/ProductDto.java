package com.example.summerProject.model;

import javax.persistence.*;
import java.math.BigDecimal;

public class ProductDto {

    public ProductDto(){}

    private Integer id;

    private String name;

    private BigDecimal price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice(){ return price; }

    public void setPrice(BigDecimal price){ this.price = price; }

}
