package com.example.summerProject.Controllers;

import com.example.summerProject.mapping.CustomerMapper;
import com.example.summerProject.mapping.ProductMapper;
import com.example.summerProject.model.Customer;
import com.example.summerProject.model.CustomerDto;
import com.example.summerProject.model.Product;
import com.example.summerProject.model.ProductDto;
import com.example.summerProject.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Tag(name="Контроллер продуктов", description="Этот контроллер отвечает за CRUD операции с объектом типа Product.")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(value = "/products")
    @Operation(
            summary = "Создание товара",
            description = "Позволяет создать запись о товаре."
    )
    public ResponseEntity<?> create(@RequestBody ProductDto productDto) {
        Product product = ProductMapper.INSTANCE.backFromDto(productDto);
        productService.create(product);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/products")
    @Operation(
            summary = "Вывод данных о всех товарах",
            description = "Позволяет вывести данные обо всех товарах."
    )
    public ResponseEntity<List<ProductDto>> findAll() {
        List<Product> products = productService.readAll();

        List<ProductDto> productDtos = new ArrayList<>();
        for(Product value : products){
            productDtos.add(ProductMapper.INSTANCE.toDto(value));
        }

        return new ResponseEntity<>(productDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/products/{id}")
    @Operation(
            summary = "Вывод данных о конкретном товаре",
            description = "Позволяет вывести данные о конкретном товаре."
    )
    public ResponseEntity<ProductDto> findById(@PathVariable(name = "id") @Parameter(description = "Идентификатор товара") int id) {
        Product product = productService.read(id);
        ProductDto productDto = ProductMapper.INSTANCE.toDto(product);

        return productDto != null
                ? new ResponseEntity<>(productDto, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/products/{id}")
    @Operation(
            summary = "Изменение данных о конкретном товаре",
            description = "Позволяет изменить данные о конкретном товаре."
    )
    public ResponseEntity<?> update(@PathVariable(name = "id") @Parameter(description = "Идентификатор товара") int id, @RequestBody ProductDto productDto) {
        Product product = ProductMapper.INSTANCE.backFromDto(productDto);
        boolean updateFlag = productService.update(product, id);

        return updateFlag
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/products/{id}")
    @Operation(
            summary = "Удаление данных о конкретном товаре",
            description = "Позволяет удалить данные о конкретном товаре."
    )
    public ResponseEntity<?> delete(@PathVariable(name = "id") @Parameter(description = "Идентификатор товара") int id) {
        boolean deleted = productService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}