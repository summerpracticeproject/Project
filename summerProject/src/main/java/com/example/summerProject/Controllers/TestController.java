package com.example.summerProject.Controllers;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Hidden
@Tag(name="Тестовый контроллер", description="Это тестовый контроллер, который возвращает ответ ОК на все запросы по адресу /test")
public class TestController {

    @RequestMapping(value = "/test")
    public String getTestData() {
        return "OK";
    }
}
