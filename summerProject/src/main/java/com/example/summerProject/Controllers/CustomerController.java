package com.example.summerProject.Controllers;

import com.example.summerProject.mapping.CustomerMapper;
import com.example.summerProject.model.Customer;
import com.example.summerProject.model.CustomerDto;
import com.example.summerProject.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@Tag(name="Контроллер клиентов", description="Этот контроллер отвечает за CRUD операции с объектом типа Customer.")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping(value = "/customers")
    @Operation(
            summary = "Создание клиента",
            description = "Позволяет создать запись о клиенте"
    )
    public ResponseEntity<?> create(@RequestBody CustomerDto customerdto) {
        Customer customer = CustomerMapper.INSTANCE.backFromDto(customerdto);
        customerService.create(customer);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/customers")
    @Operation(
            summary = "Вывод данных всех клиентов",
            description = "Позволяет вывести данные всех клиентов из базы данных."
    )
    public ResponseEntity<List<CustomerDto>> findAll() {
        List<Customer> customers = customerService.readAll();

        List<CustomerDto> customerDtos = new ArrayList<>();
        for(Customer value : customers){
            customerDtos.add(CustomerMapper.INSTANCE.toDto(value));
        }

        return new ResponseEntity<>(customerDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/customers/{id}")
    @Operation(
            summary = "Вывод данных конкретного клиента",
            description = "Позволяет вывести данные о конкретном клиенте по его id"
    )
    public ResponseEntity<CustomerDto> findById(@PathVariable(name = "id") @Parameter(description = "Идентификатор клиента") int id) {
        Customer customer = customerService.read(id);
        CustomerDto customerDto = CustomerMapper.INSTANCE.toDto(customer);
        return customerDto != null
                ? new ResponseEntity<>(customerDto, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/customers/{id}")
    @Operation(
            summary = "Обновление клиента",
            description = "Позволяет обновить данные о конкретном клиенте"
    )
    public ResponseEntity<?> update(@PathVariable(name = "id") @Parameter(description = "Идентификатор клиента") int id, @RequestBody CustomerDto customerDto) {
        Customer customer = CustomerMapper.INSTANCE.backFromDto(customerDto);
        boolean updateFlag = customerService.update(customer, id);

        return updateFlag
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/customers/{id}")
    @Operation(
            summary = "Удаление клиента",
            description = "Позволяет удалить данные о конкретном клиенте"
    )
    public ResponseEntity<?> delete(@PathVariable(name = "id") @Parameter(description = "Идентификатор клиента") int id) {
        boolean deleted = customerService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}

