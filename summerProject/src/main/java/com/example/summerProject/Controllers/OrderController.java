package com.example.summerProject.Controllers;


import com.example.summerProject.mapping.OrderMapper;
import com.example.summerProject.model.*;
import com.example.summerProject.service.CustomerService;
import com.example.summerProject.service.OrderService;
import com.example.summerProject.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Tag(name="Контроллер заказов", description="Этот контроллер отвечает за CRUD операции с объектом типа Order.")
public class OrderController {

    private final OrderService orderService;
    private final CustomerService customerService;
    private final ProductService productService;

    @Autowired
    public OrderController(OrderService orderService, CustomerService customerService, ProductService productService) {
        this.orderService = orderService;
        this.customerService = customerService;
        this.productService = productService;
    }

    /*@PostMapping(value = "/orders")
    @Operation(
            summary = "Создание товара",
            description = "Позволяет создать запись о товаре."
    )
    public ResponseEntity<?> create(@RequestBody OrderDto orderDto) {
        try {
            Order order = OrderMapper.INSTANCE.backFromDto(orderDto);
            orderService.create(order);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }*/

    @Operation(
            summary = "Создание заказа при помощи планировщика.",
            description = "Позволяет создать запись о заказе при помощи планировщика."
    )
    @Scheduled(fixedRate = 30000, initialDelay = 30000)
    public void sheduledTask(){
        try {
            Order order = new Order();
            List<Product> productList = productService.readAll();
            List<Customer> customerList = customerService.readAll();
            int i = (int) (Math.random() * productList.size());
            order.setProductID(productList.get(i).getId());
            i = (int) (Math.random() * customerList.size());
            order.setCustomerID(customerList.get(i).getId());
            orderService.create(order);
        }catch (Exception e){
        }
    }

    @GetMapping(value = "/orders")
    @Operation(
            summary = "Вывод данных о всех заказах",
            description = "Позволяет вывести данные обо всех заказах."
    )
    public ResponseEntity<List<OrderDto>> findAll() {
        List<Order> orders = orderService.readAll();

        List<OrderDto> orderDtos = new ArrayList<>();
        for(Order value : orders){
            orderDtos.add(OrderMapper.INSTANCE.toDto(value));
        }

        return new ResponseEntity<>(orderDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/orders/{id}")
    @Operation(
            summary = "Вывод данных о конкретном заказе",
            description = "Позволяет вывести данные о конкретном заказе."
    )
    public ResponseEntity<OrderDto> findById(@PathVariable(name = "id") @Parameter(description = "Идентификатор товара") int id) {
        Order order = orderService.read(id);
        OrderDto orderDto = OrderMapper.INSTANCE.toDto(order);

        return orderDto != null
                ? new ResponseEntity<>(orderDto, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/orders/{id}")
    @Operation(
            summary = "Изменение данных о конкретном заказе",
            description = "Позволяет изменить данные о конкретном заказе."
    )
    public ResponseEntity<?> update(@PathVariable(name = "id") @Parameter(description = "Идентификатор товара") int id, @RequestBody OrderDto orderDto) {
        Order order = OrderMapper.INSTANCE.backFromDto(orderDto);
        boolean updateFlag = orderService.update(order, id);

        return updateFlag
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/orders/{id}")
    @Operation(
            summary = "Удаление данных о конкретном заказе",
            description = "Позволяет удалить данные о конкретном заказе."
    )
    public ResponseEntity<?> delete(@PathVariable(name = "id") @Parameter(description = "Идентификатор товара") int id) {
        boolean deleted = orderService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}