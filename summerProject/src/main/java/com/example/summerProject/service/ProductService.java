package com.example.summerProject.service;

import com.example.summerProject.model.Product;

import java.util.List;

public interface ProductService {
    /**
     * Создает новый товар
     * @param product - Товар для создания
     */
    void create(Product product);

    /**
     * Возвращает список всех имеющихся товаров
     * @return список товаров
     */
    List<Product> readAll();

    /**
     * Возвращает товар по его ID
     * @param id - ID товара
     * @return - объект товара с заданным ID
     */
    Product read(int id);

    /**
     * Обновляет товар с заданным ID,
     * в соответствии с переданным товаром
     * @param product - товар в соответсвии с которым нужно обновить данные
     * @param id - id товара который нужно обновить
     * @return - true если данные были обновлены, иначе false
     */
    boolean update(Product product, int id);

    /**
     * Удаляет товар с заданным ID
     * @param id - id товара, который нужно удалить
     * @return - true если товар был удален, иначе false
     */
    boolean delete(int id);
}
