package com.example.summerProject.service;

import com.example.summerProject.model.Order;

import java.util.List;

public interface OrderService {

    /**
     * Создает новый заказ
     * @param order - Заказ для создания
     */
    void create(Order order) throws Exception;

    /**
     * Возвращает список всех имеющихся заказов
     * @return список заказов
     */
    List<Order> readAll();

    /**
     * Возвращает заказ по его ID
     * @param id - ID заказа
     * @return - объект заказа с заданным ID
     */
    Order read(int id);

    /**
     * Обновляет заказ с заданным ID,
     * в соответствии с переданным заказом
     * @param order - заказ в соответсвии с которым нужно обновить данные
     * @param id - id заказа который нужно обновить
     * @return - true если данные были обновлены, иначе false
     */
    boolean update(Order order, int id);

    /**
     * Удаляет заказ с заданным ID
     * @param id - id заказа, который нужно удалить
     * @return - true если заказ был удален, иначе false
     */
    boolean delete(int id);

}
