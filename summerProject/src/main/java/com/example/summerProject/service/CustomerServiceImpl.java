package com.example.summerProject.service;

import com.example.summerProject.model.Customer;
import com.example.summerProject.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public void create(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public List<Customer> readAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer read(int id) {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public boolean update(Customer customer, int id) {
        if (customerRepository.existsById(id)) {
            customer.setId(id);
            customerRepository.save(customer);
            return true;
        }

        return false;
    }

    @Override
    public boolean delete(int id) {
        if (customerRepository.existsById(id)) {
            customerRepository.deleteById(id);
            return true;
        }
        return false;
    }
}