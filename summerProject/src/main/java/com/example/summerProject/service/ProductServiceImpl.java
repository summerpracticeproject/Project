package com.example.summerProject.service;

import com.example.summerProject.model.Product;
import com.example.summerProject.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public void create(Product product) {
        productRepository.save(product);
    }

    @Override
    public List<Product> readAll() {
        return productRepository.findAll();
    }

    @Override
    public Product read(int id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public boolean update(Product customer, int id) {
        if (productRepository.existsById(id)) {
            customer.setId(id);
            productRepository.save(customer);
            return true;
        }

        return false;
    }

    @Override
    public boolean delete(int id) {
        if (productRepository.existsById(id)) {
            productRepository.deleteById(id);
            return true;
        }
        return false;
    }
}