package com.example.summerProject.service;

import com.example.summerProject.model.Order;
import com.example.summerProject.repository.CustomerRepository;
import com.example.summerProject.repository.OrderRepository;
import com.example.summerProject.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.log4j.Logger;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger log = Logger.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public void create(Order order) throws Exception {
        boolean customer = customerRepository.findById(order.getCustomerID()).orElse(null) != null;
        boolean product = productRepository.findById(order.getProductID()).orElse(null) != null;
        if(customer && product) {
            orderRepository.save(order);
            log.info("Заказ был успешно создан.");
        }
        else{
            log.info("Заказ не был создан.");
            throw new Exception();
        }
    }

    @Override
    public List<Order> readAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order read(int id) {
        return orderRepository.findById(id).orElse(null);
    }

    @Override
    public boolean update(Order customer, int id) {
        if (orderRepository.existsById(id)) {
            customer.setId(id);
            orderRepository.save(customer);
            return true;
        }

        return false;
    }

    @Override
    public boolean delete(int id) {
        if (orderRepository.existsById(id)) {
            orderRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
